<?php
require "bootstrap.php";
//use Chatter\Models\Message; 
use Chatter\Models\User; 

//use Chatter\Middleware\Logging; 


$app = new \Slim\App();

//ex1

$app->get('/customers/{id}', function($request, $response, $array){
    $id = $request->getAttribute('id');
    $json = '{"1":"John", "2":"Jack","3":"Shelly","4":"Amir"}';
    $array = json_decode($json, true);

    if(array_key_exists($id, $array)){
        $name = $array[$id];
        return $response->write('Hello, '.$name);
    }
    else{
        return $response->write('Hello, id does not exist');
    }
});

//שליפת נתונים
$app->get('/users', function($request, $response,$args){
    $_user = new User();    //תקשורת עם הדטא בייס
    $users = $_user->all(); //שליפת כל הנתונים

    $payload = [];                  //יצירת מערך ריק
    foreach($users as $u){          // לולאה על כמות הנתונים
        $payload[$u->id] = [        // הכנסת ה
            "username" => $u->username,
            "email" => $u->email
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

//יצירה
$app->post('/users', function($request, $response,$args){
    $email = $request->getParsedBodyParam('email','');
    $username = $request->getParsedBodyParam('username','');
    $_user = new User();
    $_user->email = $email;
    $_user->username = $username;
    $_user->save();
       
    if($_user->id){
        $payload = ['user_id' => $_user->id];
        return $response->withStatus(201)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

//מחיקה
$app->delete('/users/{user_id}', function($request, $response,$args){
    $user = User::find($args['user_id']);
    $user->delete();
    
    if($user->exists){
        return $response->withStatus(400);
    }
    else{
       return $response->withStatus(200);
   }
});

//הוספת קבוצה
$app->post('/users/bulk', function($request, $response, $args){
    $payload = $request->getParsedBody();
        
    User::insert($payload);
    return $response->withStatus(201)->withJson($payload);
});


//$app->add(new Logging());
$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});


/*

$app->get('/messages', function($request, $response,$args){
   $_message = new Message();
   $messages =$_message->all();
   $payload = [];
  foreach($messages as $msg){
$payload[$msg->id]= [
    "body"=>$msg->body,
    "user_id"=>$msg->user_id,
    "created_at"=>$msg->created_at
];
  };

  return $response->withStatus(200)->withJson($payload);

});


$app->post('/messages', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
     $userid = $request->getParsedBodyParam('userid','');
    $_message = new Message();
    $_message->body = $message;
    $_message->user_id = $userid;
    $_message->save();

    if($_message->id){

        $payload = ['message_id'=>$_message->id];
        return $response->withStatus(201)->withJson($payload);
    }
else{

    return $response->withStatus(400);
}

});





$app->delete('/messages/{message_id}', function($request, $response,$args){
    $message= Message::find($args['message_id']);
    $message->delete();
    if($message->exsits)
    {

        return $response->withStatus(400);
    }
        else{
             return $response->withStatus(200);

        }

   });     
    

     
$app->get('/users',function($request, $response,$args){
   $_user = new User();
   $users =$_user->all();
   $payload = [];
  foreach($users as $msg){
$payload[$msg->id]= [
    "username"=>$msg->username,
    "email"=>$msg->email,
    "profile_icon"=>$msg->profile_icon
];
  };

  return $response->withStatus(200)->withJson($payload);

});


$app->post('/users', function($request, $response,$args){
    $email = $request->getParsedBodyParam('email','');
    $username = $request->getParsedBodyParam('username','');
    $password = $request->getParsedBodyParam('password','');
    $profile_icon= $request->getParsedBodyParam('profile_icon','');
    $api_key = $request->getParsedBodyParam('api_key','');
    $_user = new User();
    $_user->email = $email;
    $_user->username = $username;
    $_user->password = $password;
    $_user->profile_icon = $profile_icon;
    $_user->api_key = $api_key;

    $_user->save();

    if($_user->id){

        $payload = ['id'=>$_user->id];
        return $response->withStatus(201)->withJson($payload);
    }
else{

    return $response->withStatus(400);
}

});



$app->delete('/users/{id}', function($request, $response,$args){
    $message= User::find($args['id']);
    $message->delete();
    if($message->exsits)
    {

        return $response->withStatus(400);
    }
        else{
             return $response->withStatus(200);

        }

   });     
    
/*

$app->put('/messages/{message_id}', function($request, $response,$args){

    $message = $request->getParsedBodyParam('message','');
    $_message =  Message::find($args['message_id']);
    $_message->body =$message;

    if( $_message->save())
    {

        $payload=['message_id'=>$_message->id,"results:the messaage has been updated successfully"];
        return $response->withStatus(200)->withJson($payload);

    }
    else{
    
    return $response->withStatus(400);
    }

});

$app->post('/messages/bulk', function($request, $response,$args){

    $payload = $request->getParsedBody();
    Message::insert($payload);
    return $response->withStatus(201)->withJson($payload);
});

*/

$app->run();